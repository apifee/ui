const initialState = {
  data: {}
}

export default function reducers(state = initialState, action) {
  switch (action.type) {
    case 'apis-fetch-success':
      return {
        ...state,
        data: action.apis.reduce((accum, api) => {
          accum[api.id] = api
          return accum
        }, {})
      }

    case 'apis-fetch-detail-success':
    case 'apis-create-success':
      return {
        ...state,
        data: { ...state.data, [action.api.id]: action.api }
      }

    default:
      return state
  }
}
