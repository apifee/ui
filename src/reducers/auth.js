const initialState = {
  user: {
    id: 'fe6c8c25-c70b-4986-be8f-23c2cbc604d3',
    token:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJmZTZjOGMyNS1jNzBiLTQ5ODYtYmU4Zi0yM2MyY2JjNjA0ZDMiLCJpYXQiOjE1MjczNDk1Nzl9.ricG9mq5PywMyJ8hNotvDn4X4ipSLZJFp69awaxNr6U'
  }
}

export default function reducers(state = initialState, action) {
  switch (action.type) {
    default:
      return state
  }
}
