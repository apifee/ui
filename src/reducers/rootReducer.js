import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import requests from './requests'
import apis from './apis'
import auth from './auth'
import subscriptions from './subscriptions'

const rootReducer = combineReducers({
  routerReducer,
  auth,
  requests,
  apis,
  subscriptions
})

export default rootReducer
