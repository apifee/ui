const initialState = {
  pendingRequests: []
}

export default function reducers(state = initialState, action) {
  const { requestType } = action

  switch (action.type) {
    case 'request-start':
      return {
        ...state,
        pendingRequests: state.pendingRequests
          .filter(request => request.requestType !== requestType)
          .concat({ requestType })
      }

    case 'request-success':
    case 'request-failure':
      return {
        ...state,
        pendingRequests: state.pendingRequests.filter(
          request => request.requestType !== requestType
        )
      }

    default:
      return state
  }
}
