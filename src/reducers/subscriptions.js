const initialState = {
  data: {}
}

export default function reducers(state = initialState, action) {
  switch (action.type) {
    case 'subscriptions-fetch-success':
      return {
        ...state,
        data: action.subscriptions.reduce((accum, subscription) => {
          accum[subscription.id] = subscription
          return accum
        }, {})
      }

    case 'subscriptions-fetch-detail-success':
    case 'subscriptions-create-success':
      return {
        ...state,
        data: { ...state.data, [action.subscription.id]: action.subscription }
      }

    case 'subscriptions-contract-deployed':
      return {
        ...state,
        data: {
          ...state.data,
          [action.subscription.id]: {
            ...state.data[action.subscription.id],
            contractAddress: action.contractAddress
          }
        }
      }

    default:
      return state
  }
}
