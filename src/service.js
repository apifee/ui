// const baseURI =
//   'https://mocksvc.mulesoft.com/mocks/c8fc24fc-9e05-4db6-a448-0d81bee684d3'

const baseURI = 'http://localhost:8080/api/v1'

export function fetchAPIs(user) {
  return fetch(`${baseURI}/apis`, {
    headers: { authorization: user.token }
  }).then(response => response.json())
}

export function fetchAPIDetail(apiId, user) {
  return fetch(`${baseURI}/apis/${apiId}`, {
    headers: { authorization: user.token }
  }).then(response => response.json())
}

export function fetchAPISubscriptions(api, user) {
  return fetch(`${baseURI}/apis/${api.id}/subscriptions`, {
    headers: { authorization: user.token }
  }).then(response => response.json())
}

export function createAPI(form, user) {
  const body = {
    name: form.name,
    description: form.description,
    url: form.url,
    providerAddress: form.providerAddress,
    terms: [
      {
        valueCreditsInWei: form.creditValue,
        minCredits: form.minimumCredits,
        maxCredits: form.maximumCredits
      }
    ]
  }

  return fetch(`${baseURI}/apis`, {
    method: 'POST',
    headers: { 'content-type': 'application/json', authorization: user.token },
    body: JSON.stringify(body)
  }).then(response => response.json())
}

export function fetchSubscriptions(user) {
  return fetch(`${baseURI}/me/subscriptions`, {
    headers: { authorization: user.token }
  }).then(response => response.json())
}

export function fetchSubscriptionDetail(subscriptionId, user) {
  return fetch(`${baseURI}/me/subscriptions/${subscriptionId}`, {
    headers: { authorization: user.token }
  }).then(response => response.json())
}

export function createSubscription(api, form, user) {
  const body = {
    apiId: api.id,
    signingAddress: form.signingAddress,
    charges: [{ termsId: api.terms[0].id }]
  }

  return fetch(`${baseURI}/me/subscriptions`, {
    method: 'POST',
    headers: { 'content-type': 'application/json', authorization: user.token },
    body: JSON.stringify(body)
  }).then(response => response.json())
}

export function patchSubsciption(subscription, contractAddress, user) {
  const body = { contractAddress }

  return fetch(`${baseURI}/me/subscriptions/${subscription.id}`, {
    method: 'PATCH',
    headers: { 'content-type': 'application/json', authorization: user.token },
    body: JSON.stringify(body)
  }).then(response => response.json())
}

export function chargeSubscription(subscription, user) {
  const body = {
    termsId: subscription.api.terms[0].id
  }

  return fetch(`${baseURI}/me/subscriptions/${subscription.id}/charges`, {
    method: 'POST',
    headers: { 'content-type': 'application/json', authorization: user.token },
    body: JSON.stringify(body)
  }).then(response => response.json())
}

export function patchSubsciptionCharge(subscription, charge, user) {
  const body = { status: 'confirmed' }

  return fetch(
    `${baseURI}/me/subscriptions/${subscription.id}/charges/${charge.id}`,
    {
      method: 'PATCH',
      headers: {
        'content-type': 'application/json',
        authorization: user.token
      },
      body: JSON.stringify(body)
    }
  ).then(response => response.json())
}
