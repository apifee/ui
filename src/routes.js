import React from 'react'
import { Route, Switch } from 'react-router'

import Home from './components/Home'
import APICreate from './components/APICreate'
import APIDetail from './components/APIDetail'
import Subscriptions from './components/Subscriptions'
import SubscriptionDetail from './components/SubscriptionDetail'
import MyAPIs from './components/MyAPIs'

export default (
  <Switch>
    <Route exact path="/" component={Home} />
    <Route exact path="/apis/new" component={APICreate} />
    <Route exact path="/me/apis" component={MyAPIs} />
    <Route exact path="/apis/:apiId" component={APIDetail} />
    <Route exact path="/subscriptions" component={Subscriptions} />
    <Route exact path="/subscriptions/:subscriptionId" component={SubscriptionDetail} />
  </Switch>
)
