import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'react-router-redux'
import { Container } from 'bloomer'
import Header from './components/Header'
import store, { history } from './store'
import routes from './routes'
import registerServiceWorker from './registerServiceWorker'
import 'bulma/css/bulma.css'
import './index.css'

ReactDOM.render(
  <Provider store={store}>
      <ConnectedRouter history={history}>
      <div className="app">
        <Header />
        <Container className="main-container">{routes}</Container>
      </div>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
)
registerServiceWorker()
