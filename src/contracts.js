import SubscriptionContract from '@apifee/contracts/build/contracts/APIFeeSubscription.json'
import { getWeb3 } from './web3'

const GAS_PRICE = 30000000
const GAS_LIMIT = 4000000

export const deploySubscriptionContract = async (subscription, form) => {
  const web3 = await getWeb3()
  const accounts = await web3.eth.getAccounts()
  const account = accounts[0]
  const contract = new web3.eth.Contract(SubscriptionContract.abi)

  const initialCharge = subscription.charges[0]
  const terms = subscription.api.terms.find(
    terms => terms.id === initialCharge.termsId
  )

  const providerAddress = subscription.api.providerAddress
  const apiFeeAddress = '0x0'
  const signingAddress = form.signingAddress
  const initialChargeId = initialCharge.id
  const initialChargeTermsHash = terms.termsHash
  const initialChargeCredits = form.credits
  const transactionValue = form.credits * terms.valueCreditsInWei

  const transaction = contract
    .deploy({
      data: SubscriptionContract.bytecode,
      arguments: [
        providerAddress,
        apiFeeAddress,
        signingAddress,
        initialChargeId,
        initialChargeTermsHash,
        initialChargeCredits
      ]
    })
    .send({
      from: account,
      value: transactionValue,
      gas: GAS_LIMIT,
      gasPrice: GAS_PRICE
    })

  return { transaction }
}

export const chargeSubscriptionContract = async (
  subscription,
  charge,
  form
) => {
  const web3 = await getWeb3()
  const accounts = await web3.eth.getAccounts()
  const account = accounts[0]
  const contract = new web3.eth.Contract(
    SubscriptionContract.abi,
    subscription.contractAddress
  )
  const terms = subscription.api.terms[0]
  const chargeId = charge.id
  const termsHash = terms.termsHash
  const credits = form.credits
  const transactionValue = credits * terms.valueCreditsInWei
  const transaction = contract.methods
    .chargeCredit(chargeId, termsHash, credits)
    .send({
      from: account,
      value: transactionValue,
      gas: GAS_LIMIT,
      gasPrice: GAS_PRICE
    })

  return { transaction }
}

export const withdrawFromSubscriptionContract = async subscription => {
  const web3 = await getWeb3()
  const accounts = await web3.eth.getAccounts()
  const account = accounts[0]
  const contract = new web3.eth.Contract(
    SubscriptionContract.abi,
    subscription.contractAddress
  )
  const credits = subscription.signature.credit
  const signature = subscription.signature.signature
  const v = web3.utils.hexToNumber('0x' + signature.slice(130, 132))
  const signatureParts = {
    r: signature.slice(0, 66),
    s: '0x' + signature.slice(66, 130),
    v: v >= 27 ? v : v + 27
  }
  const transaction = contract.methods
    .withdraw(credits, signatureParts.v, signatureParts.r, signatureParts.s)
    .send({
      from: account,
      gas: GAS_LIMIT,
      gasPrice: GAS_PRICE
    })

  return { transaction }
}
