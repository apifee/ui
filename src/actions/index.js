import * as service from '../service'
import { history } from '../store'
import { requestStart, requestSuccess, requestFailure } from './requests'
import { fetchSubscriptionsSuccess } from './subscriptions'

export function fetchAPIsSuccess(apis) {
  return {
    type: 'apis-fetch-success',
    apis
  }
}

export function fetchAPIs() {
  return (dispatch, getState) => {
    const state = getState()
    const user = state.auth.user
    dispatch(requestStart('apis-fetch'))
    return service.fetchAPIs(user)
      .then(apis => {
        dispatch(fetchAPIsSuccess(apis))
        dispatch(requestSuccess('apis-fetch'))
      })
      .catch(error => dispatch(requestFailure('apis-fetch', error)))
  }
}

export function fetchAPIDetailSuccess(api) {
  return {
    type: 'apis-fetch-detail-success',
    api
  }
}

export function fetchAPIDetail(apiId) {
  return (dispatch, getState) => {
    const state = getState()
    const user = state.auth.user
    dispatch(requestStart('apis-fetch-detail'))
    return service.fetchAPIDetail(apiId, user)
      .then(api => {
        dispatch(fetchAPIDetailSuccess(api))
        dispatch(requestSuccess('apis-fetch-detail'))
        if (api.ownerId === user.id) {
          dispatch(fetchAPISubscriptions(api))
        }
      })
      .catch(error => dispatch(requestFailure('apis-fetch-detail', error)))
  }
}

export function fetchAPISubscriptions(api) {
  return (dispatch, getState) => {
    const state = getState()
    const user = state.auth.user
    dispatch(requestStart('apis-fetch-subscriptions'))
    return service
      .fetchAPISubscriptions(api, user)
      .then(subscriptions => {
        dispatch(fetchSubscriptionsSuccess(subscriptions))
        dispatch(requestSuccess('apis-fetch-subscriptions'))
      })
      .catch(error => dispatch(requestFailure('apis-fetch-subscriptions', error)))
  }
}

export function createAPISuccess(api) {
  return {
    type: 'apis-create-success',
    api
  }
}

export function createAPI(form) {
  return (dispatch, getState) => {
    const state = getState()
    const user = state.auth.user
    dispatch(requestStart('apis-create'))
    return service.createAPI(form, user)
      .then(api => {
        dispatch(createAPISuccess(api))
        dispatch(requestSuccess('apis-create'))
        history.push(`/apis/${api.id}`)
      })
      .catch(error => dispatch(requestFailure('apis-create', error)))
  }
}
