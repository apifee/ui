export function requestStart(requestType) {
  return {
    type: 'request-start',
    requestType
  }
}

export function requestSuccess(requestType) {
  return {
    type: 'request-success',
    requestType
  }
}

export function requestFailure(requestType, error) {
  return {
    type: 'request-failure',
    requestType,
    error
  }
}
