import * as service from '../service'
import { history } from '../store'
import {
  deploySubscriptionContract,
  chargeSubscriptionContract,
  withdrawFromSubscriptionContract
} from '../contracts'
import { requestStart, requestSuccess, requestFailure } from './requests'
import { fetchAPIDetail } from './'

export function fetchSubscriptionsSuccess(subscriptions) {
  return {
    type: 'subscriptions-fetch-success',
    subscriptions
  }
}

export function fetchSubscriptions() {
  return (dispatch, getState) => {
    const state = getState()
    const user = state.auth.user
    dispatch(requestStart('subscriptions-fetch'))
    return service
      .fetchSubscriptions(user)
      .then(subscriptions => {
        dispatch(fetchSubscriptionsSuccess(subscriptions))
        dispatch(requestSuccess('subscriptions-fetch'))
      })
      .catch(error => dispatch(requestFailure('subscriptions-fetch', error)))
  }
}

export function fetchSubscriptionDetailSuccess(subscription) {
  return {
    type: 'subscriptions-fetch-detail-success',
    subscription
  }
}

export function fetchSubscriptionDetail(subscriptionId) {
  return (dispatch, getState) => {
    const state = getState()
    const user = state.auth.user
    dispatch(requestStart('subscriptions-fetch-detail'))
    return service
      .fetchSubscriptionDetail(subscriptionId, user)
      .then(subscription => {
        dispatch(fetchSubscriptionDetailSuccess(subscription))
        dispatch(requestSuccess('subscriptions-fetch-detail'))
        dispatch(fetchAPIDetail(subscription.apiId))
        return subscription
      })
      .catch(error =>
        dispatch(requestFailure('subscriptions-fetch-detail', error))
      )
  }
}

export function createSubscriptionSuccess(subscription) {
  return {
    type: 'subscriptions-create-success',
    subscription
  }
}

export function subscriptionContractDeployed(subscription, contractAddress) {
  return {
    type: 'subscriptions-contract-deployed',
    subscription,
    contractAddress
  }
}

export function createSubscription(form, api) {
  return (dispatch, getState) => {
    const state = getState()
    const user = state.auth.user
    dispatch(requestStart('subscriptions-create'))
    return service
      .createSubscription(api, form, user)
      .then(async subscription => {
        const { transaction } = await deploySubscriptionContract(
          subscription,
          form
        )
        await new Promise((resolve, reject) => {
          transaction.on('transactionHash', () => resolve())
          transaction.on('receipt', async receipt => {
            await service.patchSubsciption(
              subscription,
              receipt.contractAddress,
              user
            )
            dispatch(
              subscriptionContractDeployed(
                subscription,
                receipt.contractAddress
              )
            )
            dispatch(fetchSubscriptionDetail(subscription.id))
          })
          transaction.on('error', () => reject())
        })
        return subscription
      })
      .then(subscription => {
        dispatch(createSubscriptionSuccess(subscription))
        dispatch(requestSuccess('subscriptions-create'))
        history.push(`/subscriptions/${subscription.id}`)
        return subscription
      })
      .catch(
        error =>
          alert(error) ||
          dispatch(requestFailure('subscriptions-create', error))
      )
  }
}

export function chargeSubscriptionSuccess(subscription, charge) {
  return {
    type: 'subscriptions-charge-success',
    subscription,
    charge
  }
}

export function chargeSubscription(form, subscription) {
  return (dispatch, getState) => {
    const state = getState()
    const user = state.auth.user
    dispatch(requestStart('subscriptions-charge'))
    return service
      .chargeSubscription(subscription, user)
      .then(async charge => {
        const { transaction } = await chargeSubscriptionContract(
          subscription,
          charge,
          form
        )
        await new Promise((resolve, reject) => {
          transaction.on('receipt', async receipt => {
            await service.patchSubsciptionCharge(subscription, charge, user)
            resolve()
          })
          transaction.on('error', () => reject())
        })
        return charge
      })
      .then(charge => {
        dispatch(chargeSubscriptionSuccess(subscription, charge))
        dispatch(requestSuccess('subscriptions-charge'))
      })
      .then(() => dispatch(fetchSubscriptionDetail(subscription.id)))
      .catch(
        error =>
          alert(error) ||
          dispatch(requestFailure('subscriptions-charge', error))
      )
  }
}

export function withdraw(subscription) {
  return async dispatch => {
    const { transaction } = await withdrawFromSubscriptionContract(subscription)
    await new Promise((resolve, reject) => {
      transaction.on('receipt', async receipt => {
        resolve()
      })
      transaction.on('error', () => reject())
    })
  }
}
