import { connect } from 'react-redux'
import { fetchSubscriptions } from '../../actions/subscriptions'
import Subscriptions from './Subscriptions'

const mapStateToProps = state => ({
  subscriptions: Object.keys(state.subscriptions.data).map(
    subscriptionId => state.subscriptions.data[subscriptionId]
  )
})

const mapDispatchToProps = dispatch => ({
  fetchSubscriptions: () => dispatch(fetchSubscriptions())
})

export default connect(mapStateToProps, mapDispatchToProps)(Subscriptions)
