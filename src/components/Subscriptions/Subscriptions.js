import React, { PureComponent } from 'react'
import { Link } from 'react-router-dom'
import { Title, Box } from 'bloomer'
import './Subscriptions.css'

export default class Subscriptions extends PureComponent {
  componentDidMount() {
    const { fetchSubscriptions } = this.props
    fetchSubscriptions()
  }

  render() {
    const { subscriptions } = this.props
    return (
      <section>
        <Title>My subscriptions</Title>
        <ul>
          {subscriptions &&
            subscriptions.map(subscription => (
              <li key={subscription.id}>
                <Link to={`/subscriptions/${subscription.id}`}>
                  <Box className="subscriptions-list-box">
                    <div>
                      <span className="subscriptions-list-name">
                        {subscription.api.name}
                      </span>
                      <span className="subscriptions-list-url">
                        {subscription.api.url}
                      </span>
                    </div>
                    <p>
                      <span className="detail-label">Total credits: </span>
                      <span className="detail-value">
                        {subscription.totalCredits}
                      </span>
                      &nbsp;-&nbsp;
                      <span className="detail-label">Available credits: </span>
                      <span className="detail-value">
                        {subscription.totalCredits -
                          subscription.consumedCredits}
                      </span>
                    </p>
                  </Box>
                </Link>
              </li>
            ))}
        </ul>
      </section>
    )
  }
}
