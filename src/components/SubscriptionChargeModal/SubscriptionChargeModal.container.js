import { connect } from 'react-redux'
import SubscriptionChargeModal from './SubscriptionChargeModal'
import { chargeSubscription } from '../../actions/subscriptions'

const mapDispatchToProps = dispatch => ({
  chargeSubscription: (form, subscription) =>
    dispatch(chargeSubscription(form, subscription))
})

export default connect(null, mapDispatchToProps)(SubscriptionChargeModal)
