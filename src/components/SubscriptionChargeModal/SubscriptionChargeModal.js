import React, { PureComponent } from 'react'
import {
  Modal,
  ModalBackground,
  ModalContent,
  Box,
  Subtitle,
  Field,
  Label,
  Control,
  Input,
  TextArea,
  Button
} from 'bloomer'
import SubscriptionCredits from '../SubscriptionCredits'

import './SubscriptionChargeModal.css'

export default class SubscriptionChargeModal extends PureComponent {
  state = {
    form: {
      credits: 1
    }
  }

  componentDidMount() {
    const { subscription } = this.props
    const { form } = this.state
    if (subscription) {
      this.setState({
        form: {
          ...form,
          credits: subscription.api.terms[0].minCredits
        }
      })
    }
  }

  componentDidUpdate(prevProps) {
    const { subscription } = this.props
    const { form } = this.state
    if (subscription && subscription !== prevProps.subscription) {
      this.setState({
        form: {
          ...form,
          credits: subscription.api.terms[0].minCredits
        }
      })
    }
  }

  onFormChange = event => {
    this.setState({
      form: {
        ...this.state.form,
        [event.target.name]: event.target.value
      }
    })
  }

  onCancel = () => {
    const { onClose } = this.props
    onClose()
  }

  onAddCredits = async () => {
    const { chargeSubscription, subscription, onClose } = this.props
    const { form } = this.state
    if (this.validateForm(form)) {
      await chargeSubscription(form, subscription)
      onClose()
    }
  }

  validateForm = form => {
    const { subscription } = this.props
    const api = subscription.api
    const terms = api.terms[0]
    return form.credits >= +terms.minCredits && form.credits <= +terms.maxCredits
  }

  render() {
    const { subscription } = this.props
    const { form } = this.state
    const api = subscription.api

    return (
      <Modal isActive className="subscription-charge-modal">
        <ModalBackground className="subscription-charge-modal-background" />
        <ModalContent>
          <Box className="subscription-charge-modal-box">
            <Subtitle isSize="4">Add credits to subscription</Subtitle>

            <form className="subscription-charge-modal-form">
              <SubscriptionCredits
                terms={api.terms[0]}
                form={form}
                onChange={this.onFormChange}
              />
              <Field
                isGrouped
                isPulled="right"
                className="subscription-charge-modal-buttons"
              >
                <Control>
                  <Button onClick={this.onCancel}>Cancel</Button>
                </Control>
                <Control>
                  <Button isColor="info" onClick={this.onAddCredits}>
                    Add credits
                  </Button>
                </Control>
              </Field>
            </form>
          </Box>
        </ModalContent>
      </Modal>
    )
  }
}
