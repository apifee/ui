import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { Title, Button, Box } from 'bloomer'
import APIListItem from '../APIListItem'
import './Home.css'

export default class Home extends PureComponent {
  static propTypes = {
    apis: PropTypes.array
  }

  componentDidMount() {
    const { fetchAPIs } = this.props
    fetchAPIs()
  }

  onCreateAPI = () => {
    const { history } = this.props
    history.push('/apis/new')
  }

  render() {
    const { apis } = this.props
    return (
      <div>
        <div className="apis-title-row">
          <Title>Discover APIs</Title>
          <Button isColor="info" onClick={this.onCreateAPI}>
            Create API
          </Button>
        </div>
        <ul>
          {apis.map(api => (
            <li key={api.id}>
              <Link to={`/apis/${api.id}`}>
                <APIListItem api={api} />
              </Link>
            </li>
          ))}
        </ul>
      </div>
    )
  }
}
