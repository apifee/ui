import { connect } from 'react-redux'
import Home from './Home'
import { fetchAPIs } from '../../actions'

const mapStateToProps = (state) => ({
  apis: Object.keys(state.apis.data).map(apiId => state.apis.data[apiId])
})

const mapDispatchToProps = (dispatch) => ({
  fetchAPIs: () => dispatch(fetchAPIs())
})

export default connect(mapStateToProps, mapDispatchToProps)(Home)
