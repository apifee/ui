import React, { PureComponent } from 'react'
import {
  Field,
  Label,
  Control,
  Input
} from 'bloomer'
import SubscriptionTerms from '../SubscriptionTerms'
import './SubscriptionCredits.css'

export default class SubscriptionCredits extends PureComponent {
  render() {
    const { onChange, terms, form } = this.props
    return (
      <div className="subscription-credits-row">
        <Field>
          <Label>Credits</Label>
          <Control>
            <Input
              name="credits"
              type="number"
              value={form.credits}
              onChange={onChange}
            />
          </Control>
        </Field>
        <SubscriptionTerms terms={terms} />
      </div>
    )
  }
}
