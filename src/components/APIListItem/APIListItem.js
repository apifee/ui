import React, { PureComponent } from 'react'
import { Box } from 'bloomer'
import './APIListItem.css'

export default class APIListItem extends PureComponent {
  render() {
    const { api } = this.props
    return (
      <Box className="apis-list-box">
        <div>
          <span className="apis-list-name">{api.name}</span>
          <span className="apis-list-url">{api.url}</span>
        </div>
        <p>{api.description}</p>
      </Box>
    )
  }
}
