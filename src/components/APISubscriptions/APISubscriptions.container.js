import { connect } from 'react-redux'
import APISubscriptions from './APISubscriptions'
import { withdraw } from '../../actions/subscriptions'

const mapStateToProps = (state, ownProps) => ({
  subscriptions: Object.keys(state.subscriptions.data)
    .map(apiId => state.subscriptions.data[apiId])
    .filter(subscription => subscription.api.id === ownProps.api.id)
})

const mapDispatchToProps = dispatch => ({
  withdraw: subscription => dispatch(withdraw(subscription))
})

export default connect(mapStateToProps, mapDispatchToProps)(APISubscriptions)
