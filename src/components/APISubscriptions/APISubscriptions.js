import React, { PureComponent } from 'react'
import { Subtitle, Button, Table } from 'bloomer'
import './APISubscriptions.css'

export default class APISubscriptions extends PureComponent {
  onWithdraw = subscription => {
    const { withdraw } = this.props
    withdraw(subscription)
  }

  render() {
    const { api, subscriptions } = this.props
    return (
      subscriptions.length > 0 && (
        <div className="apis-detail-subscriptions">
          <Subtitle isSize="5" className="apis-detail-subscriptions-title">API subscriptions</Subtitle>
          <Table>
            <thead>
              <tr>
                <th>Contract</th>
                <th>Credits pending withdraw</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {subscriptions
                .filter(subscription => !!subscription.contractAddress)
                .map(subscription => (
                  <tr>
                    <td>
                      <a
                        href={`https://etherscan.io/address/${
                          subscription.contractAddress
                        }`}
                        target="_blank"
                      >
                        View on Etherscan
                      </a>
                    </td>
                    <td className="apis-detail-subscription-table-credits">{subscription.creditsPendingWithdraw}</td>
                    <td>
                      {subscription.creditsPendingWithdraw > 0 && (
                        <Button
                          isColor="info"
                          isSize="small"
                          onClick={() => this.onWithdraw(subscription)}
                          className="apis-detail-subscription-detail-button"
                        >
                          Withdraw
                        </Button>
                      )}
                    </td>
                  </tr>
                ))}
            </tbody>
          </Table>
        </div>
      )
    )
  }
}
