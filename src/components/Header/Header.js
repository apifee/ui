import React, { PureComponent } from 'react'
import { Link } from 'react-router-dom'
import {
  Container,
  Navbar,
  NavbarBrand,
  NavbarItem,
  NavbarMenu,
  NavbarStart
} from 'bloomer'

export default class Header extends PureComponent {
  render() {
    return (
      <header>
        <Navbar>
          <Container>
            <NavbarBrand>
              <NavbarItem>APIFee</NavbarItem>
            </NavbarBrand>
            <NavbarMenu>
              <NavbarStart>
                <NavbarItem>
                  <Link to="/">Discover</Link>
                </NavbarItem>
                <NavbarItem>
                  <Link to="/me/apis">My APIs</Link>
                </NavbarItem>
                <NavbarItem>
                  <Link to="/subscriptions">My subscriptions</Link>
                </NavbarItem>
              </NavbarStart>
            </NavbarMenu>
          </Container>
        </Navbar>
      </header>
    )
  }
}
