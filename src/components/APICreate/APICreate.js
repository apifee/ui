import React, { PureComponent } from 'react'
import {
  Title,
  Columns,
  Column,
  Field,
  Label,
  Control,
  Input,
  TextArea,
  Button
} from 'bloomer'

export default class APICreate extends PureComponent {
  state = {
    form: {
      name: '',
      description: '',
      url: '',
      providerAddress: '',
      creditValue: '2000000000000',
      minimumCredits: '10',
      maximumCredits: '1000000000'
    }
  }

  onFormChange = event => {
    this.setState({
      form: {
        ...this.state.form,
        [event.target.name]: event.target.value
      }
    })
  }

  onCancel = () => {
    const { history } = this.props
    history.push('/')
  }

  onCreate = () => {
    const { createAPI } = this.props
    const { form } = this.state
    if (this.validateForm(form)) {
      createAPI(form)
    }
  }

  validateForm = (form) => {
    return (
      !!form.name &&
      !!form.url &&
      !!form.description &&
      form.creditValue > 0 &&
      form.minimumCredits > 0 &&
      form.maximumCredits > form.minimumCredits
    )
  }

  render() {
    const { form } = this.state
    return (
      <section>
        <Title>Create API</Title>
        <Columns>
          <Column isSize="1/2">
            <form>
              <Field>
                <Label>Name</Label>
                <Control>
                  <Input
                    name="name"
                    value={form.name}
                    onChange={this.onFormChange}
                  />
                </Control>
              </Field>
              <Field>
                <Label>Description</Label>
                <Control>
                  <TextArea
                    name="description"
                    value={form.description}
                    onChange={this.onFormChange}
                  />
                </Control>
              </Field>
              <Field>
                <Label>Gateway URL</Label>
                <Control>
                  <Input
                    name="url"
                    value={form.url}
                    onChange={this.onFormChange}
                  />
                </Control>
              </Field>
              <Field>
                <Label>Provider address</Label>
                <Control>
                  <Input
                    name="providerAddress"
                    value={form.providerAddress}
                    onChange={this.onFormChange}
                  />
                </Control>
              </Field>
              <Field>
                <Label>Credit value (in WEI)</Label>
                <Control>
                  <Input
                    type="number"
                    name="creditValue"
                    value={form.creditValue}
                    onChange={this.onFormChange}
                  />
                </Control>
              </Field>
              <Field>
                <Label>Minimum credits</Label>
                <Control>
                  <Input
                    type="number"
                    name="minimumCredits"
                    value={form.minimumCredits}
                    onChange={this.onFormChange}
                  />
                </Control>
              </Field>
              <Field>
                <Label>Maximum credits</Label>
                <Control>
                  <Input
                    type="number"
                    name="maximumCredits"
                    value={form.maximumCredits}
                    onChange={this.onFormChange}
                  />
                </Control>
              </Field>
              <Field isGrouped isPulled="right">
                <Control>
                  <Button onClick={this.onCancel}>Cancel</Button>
                </Control>
                <Control>
                  <Button isColor="info" onClick={this.onCreate}>
                    Create API
                  </Button>
                </Control>
              </Field>
            </form>
          </Column>
        </Columns>
      </section>
    )
  }
}
