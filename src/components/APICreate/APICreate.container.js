import { connect } from 'react-redux'
import { createAPI } from '../../actions'
import APICreate from './APICreate'

const mapDispatchToProps = (dispatch) => ({
  createAPI: (form) => dispatch(createAPI(form))
})

export default connect(null, mapDispatchToProps)(APICreate)
