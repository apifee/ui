import { connect } from 'react-redux'
import SubscriptionDetail from './SubscriptionDetail'
import { fetchSubscriptionDetail } from '../../actions/subscriptions'

const mapStateToProps = (state, ownProps) => {
  const subscription =
    state.subscriptions.data[ownProps.match.params.subscriptionId]
  const api = subscription ? state.apis.data[subscription.apiId] : null

  return {
    subscription,
    api
  }
}

const mapDispatchToProps = (dispatch, ownProps) => ({
  fetchSubscriptionDetail: () =>
    dispatch(fetchSubscriptionDetail(ownProps.match.params.subscriptionId))
})

export default connect(mapStateToProps, mapDispatchToProps)(SubscriptionDetail)
