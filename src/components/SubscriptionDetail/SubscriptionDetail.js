import React, { PureComponent } from 'react'
import { Title, Button } from 'bloomer'
import SubscriptionChargeModal from '../SubscriptionChargeModal'
import './SubscriptionDetail.css'

export default class SubscriptionDetail extends PureComponent {
  state = {
    isSubscriptionChargeModalOpen: false
  }

  componentDidMount() {
    const { fetchSubscriptionDetail } = this.props
    fetchSubscriptionDetail()
  }

  onAddCredit = () => {
    this.setState({
      isSubscriptionChargeModalOpen: true
    })
  }

  onSubscriptionChargeModalClose = () => {
    this.setState({
      isSubscriptionChargeModalOpen: false
    })
  }

  render() {
    const { subscription, api } = this.props
    const { isSubscriptionChargeModalOpen } = this.state
    const apiTerms = api ? api.terms[0] : null
    return (
      <section className="subscription-detail">
        {subscription &&
          api && (
            <div>
              <Title>Subscription to {api.name}</Title>
              <div>
                <span className="detail-label">Status:</span>
                <span className="detail-value">
                  {subscription.contractAddress ? (
                    <span>
                      Active (<a
                        href={`https://etherscan.io/address/${
                          subscription.contractAddress
                        }`}
                        target="_blank"
                        className="subscription-detail-contract-link"
                      >
                        View contract
                      </a>)
                    </span>
                  ) : (
                    'Pending contract deployment'
                  )}
                </span>
              </div>
              <div>
                <span className="detail-label">Signing address:</span>
                <span className="detail-value">
                  {subscription.signingAddress}
                </span>
              </div>
              <div>
                <span className="detail-label">Purchased credits:</span>
                <span className="detail-value">
                  {subscription.totalCredits}
                </span>
              </div>
              <div className="subscription-detail-credits-row">
                <span className="detail-label">Available credits:</span>
                <span className="detail-value">
                  {subscription.totalCredits - subscription.consumedCredits}
                </span>
                <Button
                  isColor="info"
                  className="subscription-detail-add-credit-button"
                  onClick={this.onAddCredit}
                >
                  Add credit
                </Button>
              </div>
              {isSubscriptionChargeModalOpen && (
                <SubscriptionChargeModal
                  subscription={subscription}
                  onClose={this.onSubscriptionChargeModalClose}
                />
              )}
            </div>
          )}
      </section>
    )
  }
}
