import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { Title } from 'bloomer'
import APIListItem from '../APIListItem'

export default class MyAPIs extends PureComponent {
  static propTypes = {
    apis: PropTypes.array
  }

  componentDidMount() {
    const { fetchAPIs } = this.props
    fetchAPIs()
  }

  render() {
    const { apis } = this.props
    return (
      <div>
        <Title>My APIs</Title>
        <ul>
          {apis.map(api => (
            <li key={api.id}>
              <Link to={`/apis/${api.id}`}>
                <APIListItem api={api} />
              </Link>
            </li>
          ))}
        </ul>
      </div>
    )
  }
}
