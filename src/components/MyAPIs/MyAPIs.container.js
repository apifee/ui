import { connect } from 'react-redux'
import MyAPIs from './MyAPIs'
import { fetchAPIs } from '../../actions'

const mapStateToProps = state => ({
  apis: Object.keys(state.apis.data)
    .map(apiId => state.apis.data[apiId])
    .filter(api => api.ownerId === state.auth.user.id)
})

const mapDispatchToProps = dispatch => ({
  fetchAPIs: () => dispatch(fetchAPIs())
})

export default connect(mapStateToProps, mapDispatchToProps)(MyAPIs)
