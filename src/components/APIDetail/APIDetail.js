import React, { PureComponent } from 'react'
import { Title, Button } from 'bloomer'
import SubscriptionTerms from '../SubscriptionTerms'
import SubscriptionCreateModal from '../SubscriptionCreateModal'
import APISubscriptions from '../APISubscriptions'
import './APIDetail.css'

export default class APIDetail extends PureComponent {
  state = {
    isSubscriptionCreateModalOpen: false
  }
  componentDidMount() {
    const { fetchAPIDetail } = this.props
    fetchAPIDetail()
  }

  onSubscribe = () => {
    this.setState({
      isSubscriptionCreateModal: true
    })
  }

  onSubscriptionCreateModalClose = () => {
    this.setState({
      isSubscriptionCreateModal: false
    })
  }

  render() {
    const { api, user } = this.props
    const { isSubscriptionCreateModal } = this.state
    const isOwner = api && api.ownerId === user.id
    return (
      <section id="apis-detail">
        {api && (
          <div>
            <div className="apis-detail-title-row">
              <Title>{api.name}</Title>
              <Button isColor="info" onClick={this.onSubscribe}>
                Subscribe to API
              </Button>
            </div>
            <p>
              <span className="detail-label">Gateway URL:</span>
              <span className="detail-value">{api.url}</span>
            </p>
            <p>{api.description}</p>
            {isOwner && <APISubscriptions api={api} />}
            {isSubscriptionCreateModal && (
              <SubscriptionCreateModal
                api={api}
                onClose={this.onSubscriptionCreateModalClose}
              />
            )}
          </div>
        )}
      </section>
    )
  }
}
