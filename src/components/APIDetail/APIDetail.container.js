import { connect } from 'react-redux'
import APIDetail from './APIDetail'
import { fetchAPIDetail } from '../../actions'

const mapStateToProps = (state, ownProps) => ({
  api: state.apis.data[ownProps.match.params.apiId],
  user: state.auth.user
})

const mapDispatchToProps = (dispatch, ownProps) => ({
  fetchAPIDetail: () => dispatch(fetchAPIDetail(ownProps.match.params.apiId))
})

export default connect(mapStateToProps, mapDispatchToProps)(APIDetail)
