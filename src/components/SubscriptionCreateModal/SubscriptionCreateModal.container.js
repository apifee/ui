import { connect } from 'react-redux'
import SubscriptionCreateModal from './SubscriptionCreateModal'
import { createSubscription } from '../../actions/subscriptions'

const mapDispatchToProps = dispatch => ({
  createSubscription: (form, api) => dispatch(createSubscription(form, api))
})

export default connect(null, mapDispatchToProps)(SubscriptionCreateModal)
