import React, { PureComponent } from 'react'
import {
  Modal,
  ModalBackground,
  ModalContent,
  Box,
  Subtitle,
  Field,
  Label,
  Control,
  Input,
  TextArea,
  Button
} from 'bloomer'
import SubscriptionCredits from '../SubscriptionCredits'
import './SubscriptionCreateModal.css'

export default class SubscriptionCreateModal extends PureComponent {
  state = {
    form: {
      credits: 1,
      signingAddress: ''
    }
  }

  componentDidMount() {
    const { api } = this.props
    const { form } = this.state
    if (api) {
      this.setState({
        form: {
          ...form,
          credits: api.terms[0].minCredits
        }
      })
    }
  }

  componentDidUpdate(prevProps) {
    const { api } = this.props
    const { form } = this.state
    if (api && api !== prevProps.api) {
      this.setState({
        form: {
          ...form,
          credits: api.terms[0].minCredits
        }
      })
    }
  }

  onFormChange = event => {
    this.setState({
      form: {
        ...this.state.form,
        [event.target.name]: event.target.value
      }
    })
  }

  onCancel = () => {
    const { onClose } = this.props
    onClose()
  }

  onCreateSubscription = () => {
    const { form } = this.state
    const { createSubscription, api } = this.props
    if (this.validateForm(form)) {
      createSubscription(form, api)
    }
  }

  validateForm = form => {
    const { api } = this.props
    const terms = api.terms[0]
    return (
      !!form.signingAddress &&
      form.credits >= terms.minCredits &&
      form.credits <= terms.maxCredits
    )
  }

  render() {
    const { api } = this.props
    const { form } = this.state

    return (
      <Modal isActive className="subscription-create-modal">
        <ModalBackground className="subscription-create-modal-background" />
        <ModalContent>
          <Box className="subscription-create-modal-box">
            <Subtitle isSize="4">Subscribe to API</Subtitle>

            <form className="subscription-create-modal-form">
              <Field>
                <Label>Signing address</Label>
                <Control>
                  <Input
                    name="signingAddress"
                    value={form.signingAddress}
                    onChange={this.onFormChange}
                  />
                </Control>
              </Field>
              <SubscriptionCredits
                terms={api.terms[0]}
                form={form}
                onChange={this.onFormChange}
              />
              <Field
                isGrouped
                isPulled="right"
                className="subscription-create-modal-buttons"
              >
                <Control>
                  <Button onClick={this.onCancel}>Cancel</Button>
                </Control>
                <Control>
                  <Button isColor="info" onClick={this.onCreateSubscription}>
                    Subscribe to API
                  </Button>
                </Control>
              </Field>
            </form>
          </Box>
        </ModalContent>
      </Modal>
    )
  }
}
