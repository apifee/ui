import React, { PureComponent } from 'react'
import { Subtitle, Table } from 'bloomer'
import './SubscriptionTerms.css'

export default class SubscriptionTerms extends PureComponent {
  render() {
    const { terms, title } = this.props
    return (
      <div className="subscription-terms">
        <Subtitle isSize="5" className="subscription-terms-subtitle">
          {title || 'Subscription terms'}
        </Subtitle>
        <Table className="subscription-terms-table">
          <tbody>
            <tr>
              <th>Credit value (in WEI)</th>
              <td>{terms.valueCreditsInWei}</td>
            </tr>
            <tr>
              <th>Minimum credits</th>
              <td>{terms.minCredits}</td>
            </tr>
            <tr>
              <th>Maximum credits</th>
              <td>{terms.maxCredits}</td>
            </tr>
          </tbody>
        </Table>
      </div>
    )
  }
}
