import Web3 from 'web3'

let web3Instance
const GAS_PRICE = 30000000
const GAS_LIMIT = 4000000
const SELLER_ADDRESS = '0x63c5b63584c2b2ef7f440a37cfc332ff7b05af43'

function createWeb3Provider() {
  // let web3 = undefined
  let web3 = window.web3

  // Checking if Web3 has been injected by the browser (Mist/MetaMask/Cipher)
  if (typeof web3 !== 'undefined') {
    // Use Mist/MetaMask/Cipher's provider.
    web3 = new Web3(web3.currentProvider)
    console.log('Injected web3 detected.')
  } else {
    // Fallback to localhost if no web3 injection. We've configured this to
    // use the development console's port by default.
    const provider = new Web3.providers.HttpProvider('http://127.0.0.1:8545')
    web3 = new Web3(provider)
    console.log('No web3 instance injected, using Local web3.')
  }
  window.web3Instance = web3
  return web3
}

window.addEventListener('load', () => {
  web3Instance = createWeb3Provider()
})

export const getWeb3 = () =>
  new Promise((resolve, reject) => {
    if (web3Instance) {
      resolve(web3Instance)
    } else {
      window.addEventListener('load', () => {
        web3Instance = createWeb3Provider()
        resolve(web3Instance)
      })
    }
  })
